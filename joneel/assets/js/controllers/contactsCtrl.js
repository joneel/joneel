'use strict';

app.controller('ContactsCtrl', ["$scope", "$http", "$rootScope", "AuthenticationService", "$localStorage", "GetContactList", "$uibModal",
function ($scope, $http, $rootScope, AuthenticationService, $localStorage, GetContactList, $uibModal) {
	var str = "abcdefghijklmnopqrstuvwxyz";
	$scope['ASetActive'] = 'active';

	function defaultEntity(){
		return {

			firstName: '',
			lastName: '',
			middleName: '',
			preferredName: '',
			title: '',
			gender: '',
			adviser: '',
			clientType: ''
		}
	}

	$scope.contacts = {
		entity: defaultEntity(),
		icon: {
			merge: $rootScope.app.crmIcons.merge,
			addsm: $rootScope.app.crmIcons.addsm
		}
	};


	$scope.contactsFilter = {
		search: '',
		generateByAlphabet: alphabet(str),
		searchByAlphabet: 'A'
	}

	$scope.mouseEnterRow = function(index){
		//$scope[showContactEditBtn + index] = true;
		angular.element('.display-edit-item-'+index).removeClass('hide');
	}	
	$scope.mouseLeaveRow = function(index){
		angular.element('.display-edit-item-'+index).addClass('hide');
	}
	
	$scope.showMobileSearch = function(){
		$scope.searchMobile = true;
	}

	$scope.clickAlphabet = function(item){
		$scope.contactsFilter.searchByAlphabet = item; 

		angular.forEach($scope.contactsFilter.generateByAlphabet, function(val, key){
			if(item == val){
				$scope[item + 'SetActive'] = 'active';
			}
			else $scope[val + 'SetActive'] = '';
		});
		
	}

	function alphabet(str){
		return str.toUpperCase().split("");
	}

	

	//Add default values
	$scope.contactLists = [];

	$scope.addContact = function(fId, fullName, city, contact, adviser, clientType){
		$scope.contactLists.push({
			check: false,
			fullName: fullName,
			city: city,
			contact: contact,
			adviser: adviser,
			clientType: clientType,
			fId: fId	

		});
	}
	
	$scope.userCredentials = {
		username: 'testuser5@loanmarket.co.nz',
		password: "QNDBCV4J1"
	}
	function login() {
        
        AuthenticationService.Login($scope.userCredentials.username, $scope.userCredentials.password, function (result) {
            
            if (result === true) {
            	//console.log(reult);
                console.log("Loggin Successful");
            } else {
                $scope.userCredentials.error = 'Username or password is incorrect';
                console.log("Username or password is incorrect");
            }
        });
    };

    //login();
	
	GetContactList.getContacts('*').then(function(response){

		console.log(response.data);
		if(response.data){
			var data = response.data.FamilyList;
			for(var i=0; i < data.length; i++){
				$scope.addContact(data[i].FamilyID, data[i].FamilyFullName, data[i].City, data[i].Contact, data[i].Adviser, data[i].ClientType);
			}
		}
	}, function errorCallback(response){
		if(response.status === 401){
			window.location = '/';
		}
	});

	/*GetContactList.getClientInform(1058607).then(function(response){

		console.log(response.data);
		if(response.data){
			var data = response.data.FamilyList;			
		}
	});*/

	var addNewClient = {
		DOB: "1983-02-01T00:00:00+12:00",
		Email: [{
			EmailAddress: "test1@crm.com",
			Type: "Email"
		}],
		FirstName: "Michael",
		LastName: "Jordan",
		Gender: "Male",
		Phone: [
			{
				Number: "098829221",
				Type: "Mobile"
			},
			{
				Number: "000111",
				Type: "Home"
			},
			{
				Number: "000222",
				Type: "Work"
			}
		],
		PreferredName: "AirJodan23",
		Role: "Adult",
		Smoker: "No",
		Title: "Mr"

	};
	$scope.addClient = function(){
		console.log(addNewClient);

		GetContactList.addClientInform(addNewClient.DOB, addNewClient.Email, addNewClient.FirstName, addNewClient.LastName, addNewClient.Gender, addNewClient.Phone, addNewClient.PreferredName, addNewClient.Role, addNewClient.Smoker, addNewClient.Title)
		.success(function(response){
			console.log(response.data);
		})
		.error( function errorCallback(response) {
			console.log(response);
		    
		});
	}
	

	$scope.checkAllItem = function(){
		if($scope.selectedAll) $scope.selectedAll = true;
		else $scope.selectedAll = false;

		angular.forEach($scope.contactLists, function(val, key){
			val.check = $scope.selectedAll;
		});
	}

	$scope.items = ['item1', 'item2', 'item3'];
	
	//add contacts using modal form
	$scope.addContactsModal = function (size) {

        var modalInstance = $uibModal.open({
            templateUrl: 'addContactModalContent.html',
            controller: 'AddContactModalInstanceCtrl',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }                
            }
        });

        modalInstance.result.then(function (selectedItem) {
            //$scope.selected = selectedItem;
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

}]);

// Please note that $uibModalInstance represents a modal window (instance) dependency.
// It is not the same as the $uibModal service used above.

app.controller('AddContactModalInstanceCtrl', ["$scope", "$uibModalInstance", "items", function ($scope, $uibModalInstance, items) {
    
    $scope.items = items;
    $scope.selected = {
        item: $scope.items[2],
       
    };

    $scope.clientInfo = {
    	title: '',
    	gender: '',
    	deceased: false,
    	time: '8:00'
    };
    $scope.showClassHint = function(){
		
		angular.element('.popover-class').removeClass('hide');
	
	}	
	$scope.hideClassHint = function(){
		
		angular.element('.popover-class').addClass('hide');
		
	}
	$scope.timeFormat = function(format){
		$scope.clientInfo.timeFormat = format;
	}
    
    $scope.onChange = function(ele){
    	if(ele == "-1"){
    		angular.element( document.querySelector( '#clientTitle' ) ).css({"color":"rgb(174, 172, 180)"});
    	}else{
    		angular.element( document.querySelector( '#clientTitle' ) ).css({"color":"#6d4d83"});
    	}
    }
    $scope.contactSelectGroup = {
    	
    	title: ["Mr", "Mrs"],
    	role: ["Adult", "Child"],
    	class: ["Class 1","Class 2","Class 3","Class 4"]

    };
	
    $scope.clientInfo.timeFormat = "AM";

     
    $scope.ok = function () {
        $uibModalInstance.close($scope.selected.item);
       
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);


app.service('GetContactList', ['$http', '$localStorage', function ($http, $localStorage) {

        var urlBase = 'https://testapi.nzfsg.co.nz/';

        this.getContacts = function (str) {
            return $http.get(urlBase+'contacts/FamilyListGet',{
            	params: {
			  		"startWith": str
			  	},
			  	headers:{
			  		"Authorization": "Bearer "+ $localStorage.currentUser.token,
			  	
			  	}
            });
        };

        this.getClientInform = function(id){
        	return $http.get(urlBase+'contacts/ClientInformGet',{
            	params: {
			  		"familyId": id
			  	},
			  	headers:{
			  		"Authorization": "Bearer "+ $localStorage.currentUser.token,
			  	
			  	}
            });
        };
        this.addClientInform = function(DOB, Email, FirstName, LastName, Gender, Phone, PreferredName, Role, Smoker, Title){
        	return $http.post(urlBase+'contacts/ContactFamilyInfoSet', {
        		data: {
			  		"familyId": '',
			  		"DOB": DOB,
			  		"Email": Email, //array[1] 
			  		"FirstName": FirstName,
			  		"LastName": LastName,
			  		"Gender": Gender,			  		
			  		"Phone": Phone, //array[3]
			  		"PreferredName": PreferredName,
			  		"Role": Role,
			  		"SmokerStatus": Smoker,
			  		"Title": Title
			  	},
			  	headers:{
			  		"Content-Type": "application/json;charset=UTF-8",
			  		"Authorization": "Bearer "+ $localStorage.currentUser.token,
			  	}

        	});
        };
       /* this.getCustomer = function (id) {
            return $http.get(urlBase + '/' + id);
        };

        this.insertCustomer = function (cust) {
            return $http.post(urlBase, cust);
        };

        this.updateCustomer = function (cust) {
            return $http.put(urlBase + '/' + cust.ID, cust)
        };

        this.deleteCustomer = function (id) {
            return $http.delete(urlBase + '/' + id);
        };

        this.getOrders = function (id) {
            return $http.get(urlBase + '/' + id + '/orders');
        };*/
    }]);


app.controller('AddContactModalCtrl', ["$scope", "$uibModal", "$log", function ($scope, $uibModal, $log) {

    $scope.items = ['item1', 'item2', 'item3'];

    $scope.open = function (size) {

        var modalInstance = $uibModal.open({
            templateUrl: 'addContactModalContent.html',
            controller: 'AddContactModalInstanceCtrl',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            //$scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
}]);



	/*$scope.addContactMockUp = function(){
		$scope.contactLists.push(
			{
				check: false,
				firstName: 'John',
				lastName: 'Doe',
				middleName: 'Lim',
				preferredName: 'JDoe',
				locationCity: 'California',
				phoneMobile: '1234567890',
				title: 'Mr.',
				gender: 'Male',
				adviser: "James",
				clientType: "Normal"

			},
			{
				check: true,
				firstName: 'Jane',
				lastName: 'Doe',
				middleName: 'Lim',
				preferredName: 'Jane',
				locationCity: 'Denver',
				phoneMobile: '1234567890',
				title: 'Mrs.',
				gender: 'Female',
				adviser: "James",
				clientType: "Newsletter"

			},
			{
				check: false,
				firstName: 'Christie',
				lastName: 'Adams',
				middleName: '',
				preferredName: '',
				locationCity: 'Whangaparaoa',
				phoneMobile: '123-456-789',
				title: 'Mrs.',
				gender: 'Female',
				adviser: "James",
				clientType: "Normal"

			},
			{
				check: false,
				firstName: 'Christie',
				lastName: 'Adams',
				middleName: '',
				preferredName: '',
				locationCity: 'Whangaparaoa',
				phoneMobile: '123-456-789',
				title: 'Mrs.',
				gender: 'Female',
				adviser: "James",
				clientType: "Normal"

			},
			{
				check: false,
				firstName: 'Christie',
				lastName: 'Adams',
				middleName: '',
				preferredName: '',
				locationCity: 'Whangaparaoa',
				phoneMobile: '123-456-789',
				title: 'Mrs.',
				gender: 'Female',
				adviser: "James",
				clientType: "Normal"

			},
			{
				check: false,
				firstName: 'Christie',
				lastName: 'Adams',
				middleName: '',
				preferredName: '',
				locationCity: 'Whangaparaoa',
				phoneMobile: '123-456-789',
				title: 'Mrs.',
				gender: 'Female',
				adviser: "James",
				clientType: "Normal"

			},
			{
				check: false,
				firstName: 'Christie',
				lastName: 'Adams',
				middleName: '',
				preferredName: '',
				locationCity: 'Whangaparaoa',
				phoneMobile: '123-456-789',
				title: 'Mrs.',
				gender: 'Female',
				adviser: "James",
				clientType: "Normal"

			},
			{
				check: false,
				firstName: 'Christie',
				lastName: 'Adams',
				middleName: '',
				preferredName: '',
				locationCity: 'Whangaparaoa',
				phoneMobile: '123-456-789',
				title: 'Mrs.',
				gender: 'Female',
				adviser: "James",
				clientType: "Normal"

			},
			{
				check: false,
				firstName: 'Christie',
				lastName: 'Adams',
				middleName: '',
				preferredName: '',
				locationCity: 'Whangaparaoa',
				phoneMobile: '123-456-789',
				title: 'Mrs.',
				gender: 'Female',
				adviser: "James",
				clientType: "Normal"

			},
			{
				check: false,
				firstName: 'Christie',
				lastName: 'Adams',
				middleName: '',
				preferredName: '',
				locationCity: 'Whangaparaoa',
				phoneMobile: '123-456-789',
				title: 'Mrs.',
				gender: 'Female',
				adviser: "James",
				clientType: "Normal"

			},
			{
				check: false,
				firstName: 'Christie',
				lastName: 'Adams',
				middleName: '',
				preferredName: '',
				locationCity: 'Whangaparaoa',
				phoneMobile: '123-456-789',
				title: 'Mrs.',
				gender: 'Female',
				adviser: "James",
				clientType: "Normal"

			},
			{
				check: false,
				firstName: 'Christie',
				lastName: 'Adams',
				middleName: '',
				preferredName: '',
				locationCity: 'Whangaparaoa',
				phoneMobile: '123-456-789',
				title: 'Mrs.',
				gender: 'Female',
				adviser: "James",
				clientType: "Normal"

			},
			{
				check: false,
				firstName: 'Jason',
				lastName: 'Saint Stevenson',
				middleName: '',
				preferredName: '',
				locationCity: 'Auckland',
				phoneMobile: '123-456-789',
				title: 'Mr.',
				gender: 'Male',
				adviser: "James",
				clientType: "Newsletter"

			}			

		);
	}*/